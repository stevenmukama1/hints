# Trees and Graphs: Explorer Maze Escape

<!-- Hint 1 -->
<details>
  <summary><b>Hint 1:</b>&nbsp;Basis of the problem</summary><br>

This problem can by solved by graph search algorithms (DFS-BFS). 

There are many different paths that can lead to a solution. However, you need the shortest path. Take this into account when choosing the search algorithm.

<b>`Hint:`&nbsp;Start from element placed at [0, 0]. Find the goal element (the one with value 9) with a graph search algorithm.<br>
<b>`Hint:`&nbsp;Keep track of the path during the search traversal.<br>

<b>`Note:`</b>&nbsp;In order to find neighbors of the current element, you can use the helper function from the previous challenge.  However, keep in mind that walls (elements with value 1) are not neighbors!<br>
<b>`Note:`</b>&nbsp;The goal element may be placed anywhere, not only at the bottom-right corner.<br>
<b>`Note:`</b>&nbsp;The result must be in cartesian format. If you use [row, column] format on tracking paths, you need to convert them to [column, row] version.<br>
  
</details>
<!-- Hint 1 -->

---

<!-- Hint 2 -->
<details>
  <summary><b>Hint 2:</b>&nbsp;Choosing the search algorithm</summary><br>

You need to choose the graph search algorithm that can find the shortest path.

Let's check the most optimal two paths of the given maze:

<img src="images/trees_and_graphs/maze-escape/maze-escape-1.png"  width="800"><br>

## Depth-first search (DFS)

The starting point ([0, 0]) has two neighbors (connected nodes) - bottom one ([1, 0]) and  right one ([0, 1]).

If you are lucky, DFS will branch out through the unvisited node [0, 1] which is the right neighbor of the starting point. In that case, it will find the shortest path at the end  (the purple path in the above pictur). 

However, if DFS branches out through the bottom right neighbor of the starting point ([1, 0])  - there is no possibility to find the shortest path. The final result will be the green path in the above picture.

Is 50% chance good enough? Not really.

> "Whatever can go wrong, will go wrong."
> -Murphy's Law

## Breath-first search (BFS)

Below illustration shows, how the breath-first search works step by step:

<img src="images/trees_and_graphs/maze-escape/maze-escape-2.png"  width="800"><br>


<b>`Hint:`&nbsp;In this challenge, the breath-first search (BFS) is the optimal solution. It finds only the shortest path.<br>
  
</details>
<!-- Hint 2 -->

---

<!-- Hint 3 -->

<details>
  <summary><b>Hint 3: [CODE]</b>&nbsp;Structure of algorithm</summary><br>


```ruby
def maze_escape(maze)
  # keep track of visited nodes
  visited = {}

  # use row-column indexes as information in BFS queue
  # the first index [0] will keep the row information
  # the second index [1] will keep the column information
  # set the starting point
  start = [0, 0]
  goal = find_goal(maze, 9)
  # initialize BFS queue with the starting point
  queue = [start]
  # continue lookup until there's no element inside queue
  until queue.empty?
    # dequeue the first element from the queue, it's the current
    current = queue.shift
    # set the current as visited
    visited[current] = true
    # if current is goal, that means we found it
    return "Shortest path has been found!" if current == goal
    # get all neighbors of the current position
    neighbors = neighbors_of(maze, current[0], current[1])	
    neighbors.each do |neighbor|
      # if the neighbor is unvisited, then enqueue
      queue.push(neighbor) unless visited[neighbor]
    end
  end
end

# returns [[row, column], [row, column], [row, column], [row, column]]
def neighbors_of(grid, row, col)
  # helper function to find neighbors of current position(row, col)
  # don't forget to check walls
end

# returns [row, column] of goal
def find_goal(maze, target)
  # helper function to find the goal in a maze
  # return [row, col] of goal point
end
```

The above BFS algorithm finds the goal point with the shortest path. The next step is returning the calculated path instead of an annotation message. In order to do that some path tracking mechanism is necessary.


<b>`Hint:`&nbsp;You can store the path inside the queue. Instead of pushing the [current_position] to the queue, you can push [current_position, current_path].<br>
<b>`Hint:`&nbsp; When you find the goal, return the current path as the result.<br>
  
</details>
<!-- Hint 3 -->

---

<!-- Hint 4 -->
<details>
  <summary><b>Hint 4:[CODE]</b>&nbsp;How to track path during the graph traversal</summary><br>

The below example shows how to keep track of path in queue:

```ruby
start = [0, 0]
# first element of the array holds a position
# second element of the array holds an array of paths previously visited
queue = [[start, [start]]]
# then you can get both value with: 
current, path = queue.shift
# current => [0, 0]
# path => [[0, 0]]
# when you push don't forget to put them into an array:
next_node = [0, 1]
next_path = path + [next_node]
queue.push([next_node, next_path])
# then get them
current, path = queue.shift
# current => [0, 1]
# path => [[0, 0], [0, 1]]
```

<b>`Hint:`&nbsp;Implement above technique into your solution.<br>

<b>`Note:`</b>&nbsp;Above explanations use [row, column] format for holding each node. The solution must be in cartesian format. (x, y) ⇒ [column, row]. You can easily convert after you found the shortest path.<br>
  
</details>
<!-- Hint 4 -->

---

<!-- Solution 1 -->
<details>
  <summary><b>[SPOILER ALERT!]</b>&nbsp;Solution (Iterative)</summary><br>

```ruby
def maze_escape(maze)
  visited = {}
  start = [0, 0]
  goal = find_goal(maze, 9)
  queue = [[start, [start]]]

  until queue.empty?
    current, path = queue.shift
    # mark the current position visited
    visited[current] = true
    # return path in cartesian format [column, row]
    return to_cartesian(path) if current == goal
    # get the neighbors of current position
    neighbors = neighbors_of(maze, current[0], current[1])
    neighbors.each do |neighbor|
      # if the neighbor is not visited yet, push that into queue
      unless visited[neighbor]
        queue.push([neighbor, path + [neighbor]]) 
      end
    end
  end
end

# helper function
def neighbors_of(grid, row, col)
end

# helper function
def find_goal(maze, target)
end

# helper function that returns given path in cartesian format
def to_cartesian(path)
end
```
  
</details>
<!-- Solution 1 -->

---

<!-- Solution 2 -->
<details>
  <summary><b>[SPOILER ALERT!]</b>&nbsp;Solution (Recursive) - only for curious 😎</summary><br>

```ruby
def maze_escape(maze)
  visited = {}
  start = [0, 0]
  goal = find_goal(maze, 9)
  queue = [[start, [start]]]
  bfs(maze, queue, visited, goal)
end

def bfs(maze, queue, visited, goal)
  return if queue.empty?
  current, path = queue.shift
  visited[current] = true
  return to_cartesian(path) if current == goal

  neighbors = neighbors_of(maze, current[0], current[1])
  neighbors.each do |neighbor|
    queue.push([neighbor, path + [neighbor]]) unless visited[neighbor]
  end
  bfs(maze, queue, visited, goal)
end

# helper function
def neighbors_of(grid, row, col)
end

# helper function
def find_goal(maze, target)
end

# helper function that returns given path in cartesian format
def to_cartesian(path)
end
```

<b>`Note:`</b>&nbsp;Iterative way to build the BFS algorithm is much easier than the recursive one. As opposed to this, recursive way to build the DFS algorithm is much easier than the iterative one. <br>
  
</details>


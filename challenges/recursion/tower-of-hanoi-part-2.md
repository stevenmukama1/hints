# Recursion: Towers of Hanoi - Part 2 

<!-- Hint 1 -->
<details>
  <summary><b>Hint 1:</b>&nbsp;Basis of the problem</summary><br>

In the previous challenge, we solved the problem when there were only two disks.

Now, we have more than two disks. But the solution won't be much different when using the Divide & Conquer approach.

As a quick reminder of Divide & Conquer:<br>
&emsp;\- divide the problem into a number of sub-problems that are smaller instances of the same problem.<br>
&emsp;\- conquer the sub-problems by solving them. If the sub-problem sizes aren’t small enough, we continue dividing them.<br>
&emsp;\- combine the solutions to the sub-problems into the solution for the original problem.

The picture below presents a basic explanation of how to divide the three-disks problem into sub-problems:<br>
<img src="images/recursion/tower-of-hanoi-part-2/hanoi-part-2-1.png"  width="800">

Assume that you have "n" disks located on the starting peg:<br>
<b>`Hint:`&nbsp;Think of solving the problem by moving "n-1" disks onto the intermediate peg.</b><br>
<b>`Hint:`&nbsp;Then move the "n-th" disk (the largest one) onto the goal peg.</b><br>
<b>`Hint:`&nbsp;For the final step, move "n-1" disks from the intermediate peg to the goal peg.</b><br>
<b>`Hint:`&nbsp;"n-1" disks may need to be solved as a sub-problem.</b>

<b>`Note:`</b>&nbsp;You need to represent each move in "start_peg->goal_peg" format.<br>
<b>`Note:`</b>&nbsp;Focus on the recursive solution. As our brains are not effective at solving recursive problems, try to write down the solution on a piece of paper first. [**This**](https://www.natashatherobot.com/recursion-factorials-fibonacci-ruby/) link will remind you how to solve recursive problems on paper.<br>

</details>
<!-- Hint 1 -->

---

<!-- Hint 2 -->
<details>
  <summary><b>Hint 2:</b>&nbsp;The order of the required moves</summary><br>

In this hint, we'll explore all the required moves of the **three disks problem** in order.

We'll handle each sub-problem in the following way:<br>
&emsp;\- **step 1:** move all the disks on top of the largest disk onto the intermediate peg<br>
&emsp;\- **step 2:** move the largest disk onto the goal peg<br>
&emsp;\- **step 3:** move all the disks (previously placed onto the intermediate peg at case 1) onto the goal peg

<img src="images/recursion/tower-of-hanoi-part-2/hanoi-part-2-2.png"  width="800"><br>

Let's have a closer look at the three disks problem:

**Sub-problem:** Move "n-1" disks onto the intermediate peg.<br>
<img src="images/recursion/tower-of-hanoi-part-2/hanoi-part-2-3.png"  width="800"><br>
<img src="images/recursion/tower-of-hanoi-part-2/hanoi-part-2-4.png"  width="800"><br>
As you see from the above image, at the two disks sub-problem the goal peg was changed.<br>
Then, you need to divide the problem again. For the next "n-1" sub-problem there will be only one disk. We already know how to solve it.

When you have finished the first step, the current situation will look like this:<br>
<img src="images/recursion/tower-of-hanoi-part-2/hanoi-part-2-5.png"  width="800">

Now, you need to follow the same steps as we followed in first case for the third case.<br>
**Sub-problem:** Move "n-1" disks from the intermediate peg to the goal peg.<br>
<img src="images/recursion/tower-of-hanoi-part-2/hanoi-part-2-6.png"  width="800">

<b>`Hint:`&nbsp;For each sub-problem, the start, intermediate or goal peg will be different</b><br>
<b>`Hint:`&nbsp;When you are doing the first step, the goal of the sub-problem will be start->intermediate</b><br>
<b>`Hint:`&nbsp;When you are doing the third step, the goal of the sub-problem will be intermediate->goal</b><br>

</details>
<!-- Hint 2 -->

---

<!-- Hint 3 -->
<details>
  <summary><b>Hint:</b>&nbsp;Structure of the recursive function</summary><br>

**"n"** is the number of disks:<br>
&emsp;\- If **"n"** is 1, the solution is trivial. Just move the disk to the goal peg.<br>
&emsp;\- For **"n > 1"**, divide the problem into smaller sub-problems, approach each problem as a two-disk solution.


```ruby
def hanoi_steps(number_of_discs)
  move_disks(number_of_discs, 1, 2, 3)
end

# use helper recursive function for better readability
# use default peg numbers for the initial call
def move_disks(number_of_discs, start, intermediate, goal)
  # base case:
  # if there's only one disk, just print the move text from start to goal
  if n == 1
    puts move_text(start, goal)
    # stop the current step
    return
  end
  # 
  # step 1: move the top disks(number_of_discs - 1) of the current sub-problem onto the intermediate peg
  # step 2: move the largest disk of the current sub-problem onto the goal peg
  # step 3: move the intermediate disks(number_of_dics - 1) of the current sub-problem onto the goal peg
end

# helper function that returns move in string format
def move_text(from, to)
  "#{from}->#{to}"
end
```

<b>`Hint:`&nbsp;When you are doing the first step, replace the intermediate and goal pegs.<b><br>
<b>`Hint:`&nbsp;When you are doing the third step, replace the intermediate and start pegs.<b><br>

</details>
<!-- Hint -->

---

<!-- Solution -->
<details>
  <summary><b>[SPOILER ALERT!]</b>&nbsp;Solution (Recursive)</summary><br>

```ruby
def hanoi_steps(number_of_discs)
  move_disk(number_of_discs, 1, 2, 3)
end

# use helper recursive function for better readability
def move_disk(number_of_discs, start, intermediate, goal)
  # base case:
  # if there's only one disk, just print the move text from start to goal
  if number_of_discs == 1
    puts move_text(start, goal)
    return
  end
  # step 1: move the n-1 disks of the current sub-problem onto the intermediate peg
  # note that, the intermediate peg of the current problem will be the goal peg of the sub-problem
  move_disk(number_of_discs - 1, start, goal, intermediate)
  # step 2: move the n-th disk of the current sub-problem onto the goal peg
  puts move_text(start, goal)
  # step 3: move the n-1 disk from the intermediate peg to the goal peg
  # note that, the intermediate peg of the current problem will be the starting peg of the sub-problem
  move_disk(number_of_discs - 1, intermediate, start, goal)
end

# helper function that returns move in string format
def move_text(from, to)
  "#{from}->#{to}"
end
```

</details>
<!-- Solution -->
